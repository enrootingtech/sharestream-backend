<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
| Authentication
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Authentication\LoginController@login');
    Route::post('register', 'Authentication\RegisterController@register');
    Route::get('logout', 'Authentication\LoginController@logout');
});

Route::apiResource('account', 'Account\AccountController');

Route::group([
    'prefix' => 'account'
], function () {
    Route::apiResource('/{account}/friends', 'FriendController');
    Route::apiResource('/{account}/notifications', 'NotificationController');
});
