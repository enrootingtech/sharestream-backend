<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Model\Account::class, 15)->create();
        factory(App\Model\Friend::class, 15)->create();
    }
}
