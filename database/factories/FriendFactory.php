<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model\Friend;
use App\Model\Account;
use Faker\Generator as Faker;

$factory->define(Friend::class, function (Faker $faker) {
    return [
        'followee'=> function(){
            return Account::all()->random();
        },
        'follower'=> function(){
            return Account::all()->random();
        }
    ];
});
