<?php
use Carbon\Carbon;

if (!function_exists('current_date')) {
    /**
     * Returns current timestamp in a readable format
     *
     * @param timestamp
     *
     * @return readable timestamp
     */

    function current_date()
    {
        return Carbon::now()->toDateTimeString();
    }
}