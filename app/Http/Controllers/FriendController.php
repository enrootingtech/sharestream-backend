<?php

namespace App\Http\Controllers;

use App\Http\Resources\Friend\FriendCollection;
use App\Model\Account;
use App\Model\Friend;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FriendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * List of all registered accounts. used for searching a specific account by name or email
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Account $account, Request $request)
    {
        $user = auth()->user();
        checkUser($user, $account);

        $audienceRequest = request()->audience;
        $audience = "everyone";
        if ($audienceRequest == null || $audienceRequest == 'friends') {
            $audience = "friends";
        }

        $userCollection = [];

        if ($audience == 'everyone') {
            $users = Account::where([
                ['verified', "=", 1],
                ['id', '!=', $user->id]
            ])->select('first_name', 'last_name', 'email', 'picture')->get();
            if ($users != null) {
                $userCollection = FriendCollection::collection($users);
            }
        } else {
            $allFriends = $account->friends;
            if (sizeof($allFriends) != 0) {
                foreach ($allFriends as $friend) {
                    $friendInfo = Account::find($friend->follower);
                    if ($friendInfo == null) {
                        continue;
                    }

                    $userCollection[] = [
                        'name' => $friendInfo->first_name . " " . $friendInfo->last_name,
                        'email' => $friendInfo->email,
                        'image' => ($friendInfo->picture != null) ? $friendInfo->picture : "",
                        'share' => true,
                    ];
                }
            }
        }

        return response([
            'users' => $userCollection,
        ], Response::HTTP_CREATED);
    }

    /**
     * When a user followers a friend, store the follower's Id and the followee's Id
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Account $account, Request $request)
    {
        //
    }

    /**
     * Display friend's details.
     *
     * @param  \App\Model\Account  $account
     * @param  \App\Model\Account  $friend
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account, Account $friend)
    {
        $user = auth()->user();
        checkUser($user, $account);

        if ($user->id == $friend->id) {
            return response([
                'success' => false,
                'error' => "Can't be checking yourself",
            ], Response::HTTP_FORBIDDEN);
        }

        if ($friend->verified == 0) {
            return response([
                'success' => false,
                'error' => "Account not available yet",
            ], Response::HTTP_FOUND);
        }

        $friendInfo = [
            'firstName' => $friend->first_name,
            'lastName' => $friend->last_name,
            'email' => $friend->email,
            'image' => ($friend->picture != null) ? $friend->picture : "",
            'joined' => $friend->created_at,
        ];

        return response([
            'data' => $friendInfo,
        ], Response::HTTP_CREATED);
    }

    /**
     * When a user unfollowers a friend, delete resource
     *
     * @param  \App\Model\Friend  $friend
     * @return \Illuminate\Http\Response
     */
    public function destroy(Friend $friend)
    {
        //
    }
}
