<?php
namespace App\Http\Controllers\Websocket;

use App\Http\Controllers\Controller;
use App\Model\Notification;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class WebSocketController extends Controller implements MessageComponentInterface
{
    protected $connections = [];
    protected $rooms;
    protected $connectedUsers = [];

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->connections = new \SplObjectStorage;
        $this->connectedUsers = [];
        $this->rooms = [];
    }

    /**
     * Get all online users and broadcast
     */
    private function onlineUsers($currentUser)
    {
        $users = [];
        $count = 0;
        foreach ($this->connections as $client) {
            if ($currentUser !== $client->resourceId) {
                $users[] = [
                    'uid' => $client->resourceId,
                ];
            }
            $count++;
        }
        $size = sizeof($users);
        echo "Online users = " . $count . "\n";
        $userData = json_encode([
            'type' => 'userlist',
            'users' => $users,
        ]);

        // Send to the requester
        $this->uniqueUser($currentUser, $userData);

    }

    /**
     * Send message to a specific user
     * @param $uid User session id
     * @param $msg Json encode message
     */
    private function uniqueUser($uid, $msg)
    {
        foreach ($this->connections as $client) {
            if ($client->resourceId === $uid) {
                $client->send($msg);
            }
        }
    }

    /**
     * @param $resourceId
     */
    protected function checkClient($resourceId)
    {
        $result = null;
        foreach ($this->connections as $client) {
            if ($resourceId == $client->resourceId) {
                $result = $client;
                break;
            }
        }
        return $result;
    }

    /**
     * Setting user details, email, full name and picture
     */
    private function setInfo($from, $data)
    {
        $data = $data->msg;
        $this->connectedUsers[$data->email] = [
            'id' => $data->id,
            'name' => $data->name,
            'picture' => $data->picture,
            'socket' => $from,
        ];
    }

    /**
     * Find clint of the given email address
     */
    private function findClient($email)
    {
        $client = null;
        if (isset($this->connectedUsers[$email])) {
            $client = $this->connectedUsers[$email];
        }
        return $client;
    }

    /**
     * On close, or disconnect, remove user from the rooms
     */
    protected function closeClientConnection($conn, $type = '')
    {
        $client = $this->checkClient($conn->resourceId);
        if ($client == null) {
            return;
        }

        $userEmail = "";
        $userId = 0;
        // Find owner of the connection
        foreach ($this->connectedUsers as $email => $userInfo) {
            if ($userInfo['socket']->resourceId == $conn->resourceId) {
                $userEmail = $email;
                $userId = (int) $userInfo['id'];
                break;
            }
        }

        foreach ($this->rooms as $roomId => $connectedClients) {

            if (isset($this->rooms[$roomId][$userEmail])) {
                $roomClient = $this->rooms[$roomId][$userEmail];
                if ($roomClient['admin']) {
                    unset($this->rooms[$roomId]);
                    // Send signal to the audience
                    $msg = [
                        'email' => $userEmail,
                        'roomId' => $roomId,
                        'admin' => true,
                        'audience' => 'everyone',
                    ];
                    $this->sendToAudience($msg, 'kill-stream');
                    $this->connectedAudience($roomId);
                    echo 'Also removed room ' . $roomId . ' thats owned by ' . $userEmail . "\n";

                    // Delete notification from database
                    if ($userId != 0) {
                        $this->removeNotification($userId, $roomId, $userEmail);
                    }

                } else {
                    unset($this->rooms[$roomId][$userEmail]);
                }
            }
            if (isset($this->rooms[$roomId])) {
                if (sizeof($this->rooms[$roomId]) == 0) {
                    unset($this->rooms[$roomId]);
                }
            }

        }
        if ($type == 'error') {
            $conn->close();
        } else {
            $this->connections->detach($conn);
        }
        // Send signal when user has an error
        $this->onlineUsers($conn->resourceId);
    }

    /**
     * Kill stream.. Send signal to the connected audience, and delete stream from notifications DB table,
     * The creator is the one to kill the stream
     */
    private function kill_stream($from, $data)
    {
        $data = $data->msg;

        if (!isset($data->roomId) || !isset($data->email)) {
            return;
        }

        if (!isset($this->connectedUsers[$data->email])) {
            echo "Sender doesn't have a registered socket";
            return;
        }

        $senderSocket = $this->connectedUsers[$data->email];

        $isCreator = false;
        if (isset($this->rooms[$data->roomId]) && isset($this->rooms[$data->roomId][$data->email])) {
            $roomClient = $this->rooms[$data->roomId][$data->email];
            if ($roomClient['admin']) {
                $isCreator = true;
            }
            unset($this->rooms[$data->roomId][$data->email]);
        } else {
            echo "Not in the room";
            return;
        }

        // Send signal to the audience
        $msg = [
            'email' => $data->email,
            'roomId' => $data->roomId,
            'admin' => $isCreator,
            'audience' => 'everyone',
        ];
        $this->sendToAudience($msg, 'kill-stream');
        $this->connectedAudience($data->roomId);

        if (!$isCreator) {
            return;
        }

        // Kill the stream, make sure only creator is the one killing the stream
        foreach ($this->rooms as $roomId => $connectedClients) {
            if (isset($this->rooms[$data->roomId]) && isset($this->rooms[$data->roomId][$data->email])) {
                unset($this->rooms[$data->roomId]);
                echo "Removed room " . $data->roomId . " owned by " . $data->email . "\n";
            }
        }

        // Delete notification from database
        $this->removeNotification($senderSocket['id'], $data->roomId, $data->email);

    }

    private function removeNotification($userId, $roomId, $email)
    {
        $notifications = Notification::where([
            ['sender', '=', $userId],
            ['type', '=', 'stream'],
        ])
            ->where('message', 'like', '%"roomId":"' . $roomId . '","email":"' . $email . '"%')
            ->get();

        if ($notifications != null) {
            foreach ($notifications as $notification) {
                Notification::where('id', $notification['id'])->delete();
            }
        }
    }

    /**
     * Broadcast to every user or selected audience
     */
    protected function sendToAudience($msg, $type)
    {
        $sentData = $msg;

        if (!isset($this->connectedUsers[$sentData['email']])) {
            echo "Sender doesn't have a registered socket";
            return;
        }

        $senderSocket = $this->connectedUsers[$sentData['email']];

        $audience = $msg['audience'];
        $current_date = current_date();

        if ($type == 'kill-stream') {
            $data = json_encode([
                'type' => $type,
                'msg' => $msg,
            ]);
        } else {
            $friends = $msg['to'];
            $data = json_encode([
                'type' => $type,
                'msg' => [
                    'media' => $sentData['bucket'],
                    'title' => $sentData['title'],
                    'roomId' => $sentData['roomId'],
                    'image' => $sentData['image'],
                    'email' => $sentData['email'],
                    'created_at' => $current_date,
                ],
            ]);
        }

        $receivers = [];
        if ($audience == 'friends') {
            if (sizeof($friends) == 0) {
                echo "Didn't select audience to broadcast to!";
                return;
            }

            foreach ($friends as $info) {
                if (!isset($this->connectedUsers[$info->email])) {
                    continue;
                }

                $friendSocket = $this->connectedUsers[$info->email]['socket'];
                if ($friendSocket != $senderSocket['socket']) {
                    $id = $this->connectedUsers[$info->email]['id'];
                    $receivers[$id] = $id;
                    $friendSocket->send($data);
                }
            }
        } else {
            foreach ($this->connectedUsers as $info) {
                if ($info['id'] != $senderSocket['id']) {
                    $receivers[$info['id']] = $info['id'];
                    $info['socket']->send($data);
                }
            }
        }

        // Save to database

        if (sizeof($receivers) == 0 || $type == 'kill-stream') {
            return;
        }

        $save = function ($receiver, $sender, $msg, $current_date) {
            $media = [];

            foreach ($msg['bucket'] as $bucke) {
                $media[] = [
                    'title' => $bucke->title,
                    'size' => $bucke->size,
                    'duration' => $bucke->duration,
                    'label' => $bucke->label,
                    'path' => $bucke->path,
                ];
            }
            $notification = new Notification;
            $notification->sender = $sender;
            $notification->receiver = $receiver;
            $notification->type = "stream";
            $notification->message = json_encode([
                'title' => $msg['title'],
                'roomId' => $msg['roomId'],
                'email' => $msg['email'],
                'image' => $msg['image'],
                'media' => $media,
                'created_at' => $current_date,
            ]);
            $notification->save();
        };

        foreach ($receivers as $receiverId) {
            $save($receiverId, $senderSocket['id'], $msg, $current_date);
        }

    }

    /**
     * Broadcast to a certain room
     */
    protected function sendToRoom($from, $msg, $type)
    {
        if (!isset($this->rooms[$msg['roomId']])) {
            echo "Room " . $msg['roomId'] . " is removed";
            return;
        } else {
            $room = $this->rooms[$msg['roomId']];
            $data = json_encode([
                'type' => $type,
                'msg' => $msg,
            ]);
            foreach ($room as $connected_email => $info) {
                if ($info['client']['socket'] != $from) {
                    $info['client']['socket']->send($data);
                }
            }
        }

    }

    protected function handleStreamRequest($from, $data)
    {
        $roomId = $data->msg->roomId;
        if ($this->findClient($data->msg->to) == null) {
            $from->send(json_encode([
                'type' => 'unable-to-join',
                'roomId' => $roomId,
            ]));
            return;
        }

        $this->connectUserToRoom($data->msg->from, $roomId);
        $from->send(json_encode([
            'type' => 'added-to-room',
            'roomId' => $roomId,
            'sessionId' => $from->resourceId,
        ]));
    }

    /**
     * @param $client
     * @param $roomId
     */
    protected function connectUserToRoom($email, $roomId)
    {
        $client = $this->findClient($email);
        if ($client == null) {
            $from->send(json_encode([
                'type' => 'not-registered',
                'roomId' => $roomId,
            ]));
            return;
        }
        if (!isset($this->rooms[$roomId])) {
            echo "Room " . $roomId . " is removed";
            $from->send(json_encode([
                'type' => 'room-removed',
                'roomId' => $roomId,
            ]));
            return;
        }
        $this->rooms[$roomId][$email] = ['client' => $client, 'admin' => false];
        $this->connectedAudience($roomId);
    }

    /**
     * Count number of Audience connected in a certain room
     * @param roomId
     */
    private function connectedAudience($roomId)
    {
        $connectedUsers = [];
        if (isset($this->rooms[$roomId])) {
            foreach ($this->rooms[$roomId] as $email => $user) {
                $client = $user['client'];
                $connectedUsers[] = [
                    'id' => $client['id'],
                    'admin' => $user['admin'],
                    'name' => $client['name'],
                    'email' => $email,
                    'picture' => $client['picture'],
                    'socket' => $client['socket'],
                ];
            }
        }

        echo "Users in the room, count = " . sizeof($connectedUsers) . "\n";
        if (sizeof($connectedUsers) != 0) {
            $data = [];
            foreach ($connectedUsers as $user) {
                $data[] = [
                    'id' => $user['id'],
                    'admin' => $user['admin'],
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'picture' => $user['picture'],
                ];
            }
            $encodeData = json_encode([
                'type' => 'online-audience',
                'msg' => $data,
            ]);
            foreach ($connectedUsers as $info) {
                $info['socket']->send($encodeData);
            }
        }
    }

    /**
     * Create Stream Room
     * @param $from The owner of the room, $data contains Media details and the audience to notify
     */
    private function createRoom($from, $data)
    {
        $roomId = '';
        while (true) {
            $roomId = $this->randomStr();
            if ($roomId == "") {
                continue;
            }

            if (!array_key_exists($roomId, $this->rooms)) {
                break;
            }
        }

        var_dump("New room: " . $roomId);
        $data = $data->msg;

        $client = $this->findClient($data->from->email);
        if ($client == null) {
            echo "You are not registered with a socket";
            return;
        }
        $this->rooms[$roomId][$data->from->email] = ['client' => $client, 'admin' => true];
        // Send to creator
        $client['socket']->send(json_encode([
            'type' => 'roomId',
            'roomId' => $roomId,
        ]));

        // then publish to audience

        $info = [
            'bucket' => $data->bucket,
            'title' => $data->from->name,
            'email' => $data->from->email,
            'image' => $client['picture'],
            'audience' => $data->audience,
            'to' => $data->to,
            'roomId' => $roomId,
        ];
        $this->sendToAudience($info, 'broadcast');
    }

    private function randomStr()
    {
        $length = 5;
        $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces[] = $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

    private function handleRoom($from, $data)
    {
        $type = $data->msg->type;
        switch ($type) {
            case "initiate-peer":
                $info = [
                    'sdp' => $data->msg->sdp,
                    'roomId' => $data->msg->roomId,
                    'from' => $from->resourceId,
                ];
                $this->sendToRoom($from, $info, 'peer-offer');
                break;
            case 'answer':
                echo "Msg from: " . $from->resourceId . "\n";
                $info = json_encode([
                    'type' => 'answer',
                    'msg' => [
                        'sdp' => $data->msg->sdp,
                    ],
                ]);
                $this->uniqueUser($data->msg->to, $info);
                break;
            case 'movie-label':
                $info = [
                    'movie' => $data->msg->movie,
                    'label' => $data->msg->label,
                    'roomId' => $data->msg->roomId,
                    'from' => $from->resourceId,
                ];
                $this->sendToRoom($from, $info, 'movie-label');
                break;
            case 'play-control':
                $info = [
                    'play' => $data->msg->play,
                    'roomId' => $data->msg->roomId,
                    'from' => $from->resourceId,
                ];
                $this->sendToRoom($from, $info, $type);
                break;
            case 'end-of-playback':
                $info = [
                    'play' => $data->msg->play,
                    'roomId' => $data->msg->roomId,
                    'from' => $from->resourceId,
                ];
                $this->sendToRoom($from, $info, $type);
                break;

        }
    }

    /**
     * When a new connection is opened it will be passed to this method
     * @param  ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws \Exception
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->connections->attach($conn);

        echo "New connection: ({$conn->resourceId})\n";
        $conn->send(json_encode([
            'id' => $conn->resourceId,
            'type' => 'id',
        ]));

        // Send signal for new online user
        $this->onlineUsers($conn->resourceId);
    }

    /**
     * Triggered when a client sends data through the socket
     * @param  \Ratchet\ConnectionInterface $conn The socket/connection that sent the message to your application
     * @param  string $msg The message received
     * @throws \Exception
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        $data = json_decode($msg);
        $type = $data->type;

        switch ($type) {
            case 'notification':
                echo "Create-Room message \n";
                // Create room and broadcast it
                $this->createRoom($from, $data);
                break;
            case 'stream-request':
                echo "Stream-Request message \n";
                $this->handleStreamRequest($from, $data);
                break;
            case 'set-info':
                echo "Set-Info message \n";
                $this->setInfo($from, $data);
                break;
            case 'room':
                echo "Room message \n";
                $this->handleRoom($from, $data);
                break;
            case 'kill-stream':
                echo "Kill-Stream message \n";
                $this->kill_stream($from, $data);
                break;
            case 'online-audience':
                $this->connectedAudience($data->msg->roomId);
                break;
            default:
                echo "Default message \n";
                foreach ($this->connections as $client) {
                    if ($from != $client) {
                        $client->send($msg);
                    }
                }
                break;
        }

    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    public function onClose(ConnectionInterface $conn)
    {
        echo "Connection {$conn->resourceId} has disconnected\n";
        $this->closeClientConnection($conn);
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param  ConnectionInterface $conn
     * @param  \Exception $e
     * @throws \Exception
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred with user {$e->getMessage()}\n";
        $this->closeClientConnection($conn, 'error');
    }

}
