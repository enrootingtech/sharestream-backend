<?php

namespace App\Http\Controllers;

use App\Model\Account;
use App\Model\Notification;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Account $account, Request $request)
    {
        $user = auth()->user();
        checkUser($user, $account);

        $notificationType = request()->type;
        $notificationState = request()->state;

        $type = "all";
        if ($notificationType != null) {
            $notificationType = strtolower($notificationType);
            if ($notificationType == 'stream') {
                $type = $notificationType;
            }
        }

        $state = "all";
        if ($notificationState != null) {
            $notificationState = strtolower($notificationState);
            if ($notificationState == 'read' || $notificationState == 'unread') {
                $state = $notificationState;
            }
        }

        $getNotifications = $account->notifications;

        if (sizeof($getNotifications) == 0) {
            return response([
                'data' => [],
            ], Response::HTTP_CREATED);
        }

        $notifications = [];
        foreach ($getNotifications as $getNotification) {
            $message = json_decode($getNotification->message);
            $continue = false;
            if ($type != "all") {
                if ($type == $getNotification->type) {
                    $continue = true;
                }
            } else {
                $continue = true;
            }

            if (!$continue) {
                continue;
            }

            if ($state != 'all') {
                if ($getNotification->state != $state) {
                    continue;
                }
            }

            $sender = Account::find($getNotification->sender);
            if ($sender == null) {
                continue;
            }

            $notifications[] = [
                'from' => [
                    'id' => $getNotification->sender,
                    'name' => $sender->first_name . " " . $sender->last_name,
                    'email' => $sender->email,
                ],
                'message' => $message,
                'type' => $getNotification->type,
                'state' => $getNotification->state,
                'received_at' => $getNotification->created_at,
            ];
        }

        return response([
            'data' => $notifications,
        ], Response::HTTP_CREATED);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        //
    }
}
