<?php

namespace App\Http\Resources\Friend;

use Illuminate\Http\Resources\Json\Resource;

class FriendCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=> $this->first_name . " " .$this->last_name,
            'email'=> $this->email,
            'image'=> ($this->picture != null) ? $this->picture : '',
            'share' => true
        ];
    }
}
