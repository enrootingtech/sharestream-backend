<?php

namespace App\Model;

use App\Model\Account;
use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{

    public function account(){
        return $this->belongsTo(Account::class);
    }
}
